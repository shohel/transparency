# ProVerif Model of the SPTP protocol

## Running Tests

Test security goals when the server is honest.
```
$ proverif cloning.pv
```

To test security goals when the server is compromised, compromise
the server private key, then run the above command

