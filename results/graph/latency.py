import matplotlib.pyplot as plt

x = [20000, 40000, 60000, 80000, 100000]

# RAFT data
# raft [{{'querybind': ['0.00802', '0.00812', '0.00827', '0.00824', '0.00822']}, {'addbind': ['0.798', '0.893', '0.743', '0.685', '1.68']}]
y0 = [0.798, 0.893, 0.743, 0.685, 1.68] # Register Binding
y1 = [0.00802, 0.00812, 0.00827, 0.00824, 0.00822] # Query Binding

# IBFT data
# ibft [{'querybind': ['0.00836', '0.00813', '0.00805', '0.00826', '0.00818']}, {'addbind': ['1.71', '1.78', '1.81', '1.79', '1.8']}]
y4 = [1.71, 1.78, 1.81, 1.79, 1.8] # Register Binding
y5 = [0.00836, 0.00813, 0.00805, 0.00826, 0.00818] # Query Binding

# Requirements
y8 = [3, 3, 3, 3, 3] # Global latency requirements, 3 sec
plt.plot(x, y8, label="Global Requirements", linestyle='dashed', marker='o')

# RAFT
plt.plot(x, y0, label="RAFT Register Binding", color='green',  marker='o')
plt.plot(x, y1, label="RAFT Query Binding", color='black', linestyle='dashdot',  marker='o' )

# IBFT
plt.plot(x, y4, label="IBFT Register Binding", color='red',  marker='o')
plt.plot(x, y5, label="IBFT Query Binding", color='orange', linestyle='dotted',  marker='o')

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.legend(bbox_to_anchor=(.05, 0.93), loc=2, ncol=2)

plt.tight_layout()

plt.show()