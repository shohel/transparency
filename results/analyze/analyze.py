import glob
from pathlib import Path
import pandas as pd

FILE_PATHS="./raft/*.csv"


DATUM=[20000, 40000, 60000, 80000, 100000]

res = {}
graphres = []
dir_name = ''

for filepath in glob.iglob(FILE_PATHS):
    result = {}
    graphresult = []
    try:
        dt = pd.read_csv(filepath)
        file_path = Path(filepath) # python > 3.4
        dir_name = file_path.parents[0]
        file_name = file_path.stem
    except KeyError:
        print ("Error reading file %s" %(filepath))
        exit(1)

    col = ['delay']
    df = pd.DataFrame(dt, columns=col)

    df_sub_copy = df.copy()

    for count in DATUM:
        df = df_sub_copy.head(count)
        av_column = df.mean(axis=0)
        format_average = '{0:.3g}'.format(av_column['delay'])
        #result[count] = format_average
        graphresult.append(format_average)

    #res[file_name] = result
    graphres.append({file_name: graphresult})
    #print( file_name, result)
    #print( "Graph List ", graphresult)

#print (res)
print (dir_name, graphres)
