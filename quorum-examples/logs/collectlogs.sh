#!/bin/bash

for i in {1..7}
do
    nodename="node${i}_1"
    echo "Collecting ${nodename} logs"
    docker logs quorum-examples_${nodename} &> ${nodename}.log
done