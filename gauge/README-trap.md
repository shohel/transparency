# Transparency experiment
Fork of gauge

## Prerequesites

### Setting up docker

```
sudo systemctl edit docker.service
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://127.0.0.1:2375

sudo systemctl daemon-reload
sudo systemctl restart docker.service
sudo netstat -lntp | grep dockerd

ps aux|grep dockerd
```

## Quickstart

```sh
make install
make deploy
make run
make run exp=transparency
make run exp=greetings
```

## Gauge setup

```sh
git clone https://github.com/jpmorganchase/quorum-examples
cd quorum-examples
Ensure docker compose is latest to support 3..
docker-compose up -d

git clone https://github.com/persistentsystems/gauge.git
vim benchmark-tool/listener/kafka-config.json
Add vm public ip address to kafka and zookeper
Update gauge/kafka-setup/docker-compose-kafka.yml: KAFKA_ADVERTISED_HOST_NAME: <public-ip>
docker-compose up -f kafka up -d

cd gauge/benchmark-tool
npm install
vim benchmark-tool/benchmark/quorum/write/config-quorum-open.json
"docker": {
            "name": [
                            "http://localhost:2375/quorum-examples_node1_1",
                                            "http://localhost:2375/quorum-examples_node2_1",
                                                            "http://localhost:2375/quorum-examples_node3_1"
                                                                    ]
},

                CURRENT_NODE=node1 npm run quorum-open
# ad configs

# deploy your contract
cd /home/ubuntu/gauge/deployment-script/quorum
CURRENT_NODE="node1" node deploy.js quorum.json
# Update deployment-script/quorum/quorum.json with
{
        "name": "BindingRegistry",
            "experimentName": "BindingRegistry",
                "path": "quorum/contracts/transparency/transparency.sol",
}
CURRENT_NODE="node1" node deploy.js quorum.json


In case of restart:
docker prune volume -f
```

## Source

* <https://docs.docker.com/install/linux/linux-postinstall/#configure-where-the-docker-daemon-listens-for-connections>
* <https://github.com/persistentsystems/gauge/blob/master/docs/output-explaination.md>
