pragma solidity ^0.4.23;

contract greeter {
    string public message;
    string public msg;
    event Finished(string str);

    constructor(string _message) public {
        message = _message;
    }

    function greet(string msg) public view returns(string) {
        Finished(message);
        return message;
    }

    function setMessage(string newMessage) public {
        message = newMessage;
        Finished(newMessage);
    }
}
