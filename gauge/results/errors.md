# Experiment Errors

## 2.2.4

panic: revision id 55 cannot be reverted

goroutine 100 [running]:
github.com/ethereum/go-ethereum/core/state.(*StateDB).RevertToSnapshot(0xc002b040d0, 0x37)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/state/statedb.go:566 +0x1a3
github.com/ethereum/go-ethereum/miner.(*worker).commitTransaction(0xc000366a80, 0xc03cedfdd0, 0x7b049b7f28de12b9, 0x8eb7b5946e432842, 0xb47210c37161e13e, 0x9d415fe6, 0x0, 0x0, 0x130f6a0, 0xc0001f97c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:759 +0x676
github.com/ethereum/go-ethereum/miner.(*worker).commitTransactions(0xc000366a80, 0xc033e79ef0, 0x7b049b7f28de12b9, 0x8eb7b5946e432842, 0x7161e13e, 0xc0200fc238, 0xbf3d51bac7922c53)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:833 +0x385
github.com/ethereum/go-ethereum/miner.(*worker).commitNewWork(0xc000366a80, 0xc0200fc238, 0x0, 0x5d14cf6b)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:1007 +0xae1
github.com/ethereum/go-ethereum/miner.(*worker).mainLoop(0xc000366a80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:430 +0xba1
created by github.com/ethereum/go-ethereum/miner.newWorker
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:228 +0x544

start client 16740:Sign Binding
[2019-06-27T14:15:24.087] [ERROR] Benchmark - Query failed, Error: missing trie node 56b44b60e08a0ae0479ec17e8e8fc79343ba351158c8fdbe56470ddbbb919e55 (path )

## Gauge Drawback

* using web3 version 0.20.^

## Potential BUG? KAFKA BROKEN ?

- Observation: with high traffic the client library seems to give negative latency !

+------+-----------------+------+------+-----------+-----------+----------------+----------------+------------+-----------+-----------+-----------+
| Test | Name            | Succ | Fail | Send Rate | Max Delay | Min Delay      | Avg Delay      | Throughput | Delay_c2e | Delay_e2o | Delay_o2v |
|------|-----------------|------|------|-----------|-----------|----------------|----------------|------------|-----------|-----------|-----------|
| 0    | BindingRegistry | 1    | 0    | 1 tps     | 0.0000 s  | -584902.4060 s | -584902.4060 s | 1 tps      | 0         | 0         | 0         |
+------+-----------------+------+------+-----------+-----------+----------------+----------------+------------+-----------+-----------+-----------+

- quorum raft consesus pending transactions breaks the experiment with

```sh
> web3.txpool.status
{
  pending: 29062,
  queued: 0
}

[2019-06-20T07:39:45.450] [ERROR] Benchmark - Query failed, Error: Invalid JSON RPC response: ""
    at Object.InvalidResponse (/home/ubuntu/gauge/benchmark-tool/node_modules/web3/lib/web3/errors.js:38:16)
    at XMLHttpRequest.request.onreadystatechange (/home/ubuntu/gauge/benchmark-tool/node_modules/web3/lib/web3/httpprovider.js:126:24)
    at XMLHttpRequestEventTarget.dispatchEvent (/home/ubuntu/gauge/benchmark-tool/node_modules/xhr2-cookies/dist/xml-http-request-event-target.js:34:22)
    at XMLHttpRequest._setReadyState (/home/ubuntu/gauge/benchmark-tool/node_modules/xhr2-cookies/dist/xml-http-request.js:208:14)
    at XMLHttpRequest._onHttpRequestError (/home/ubuntu/gauge/benchmark-tool/node_modules/xhr2-cookies/dist/xml-http-request.js:349:14)
    at ClientRequest.<anonymous> (/home/ubuntu/gauge/benchmark-tool/node_modules/xhr2-cookies/dist/xml-http-request.js:252:61)
    at emitOne (events.js:116:13)
    at ClientRequest.emit (events.js:211:7)
    at Socket.socketOnEnd (_http_client.js:423:9)
    at emitNone (events.js:111:20)
```

- Random kafka timeout

```sh
[2019-06-20T07:54:35.144] [ERROR] Benchmark - Error during publishing in kafka Error: RequestTimedOut
    at Object.<anonymous> (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/protocol/protocol.js:586:17)
    at Object.self.tap (/home/ubuntu/gauge/benchmark-tool/node_modules/binary/index.js:248:12)
    at Object.decodePartitions (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/protocol/protocol.js:584:73)
    at Object.self.loop (/home/ubuntu/gauge/benchmark-tool/node_modules/binary/index.js:267:16)
    at Object.<anonymous> (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/protocol/protocol.js:68:8)
    at Object.self.loop (/home/ubuntu/gauge/benchmark-tool/node_modules/binary/index.js:267:16)
    at decodeProduceResponse (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/protocol/protocol.js:579:6)
    at Client.handleReceivedData (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/client.js:782:20)
    at Socket.<anonymous> (/home/ubuntu/gauge/benchmark-tool/node_modules/kafka-node/lib/client.js:738:10)
    at emitOne (events.js:116:13)
```

## RAFT consensus

### 10,000

web3.txpool.status
{
	pending: 0,
	queued: 64
}


some cases
> web3.txpool.status
{
  pending: 21213,
  queued: 0
}

-> Benchmark - Error: Invalid JSON RPC response: ""

Pending transactions are transactions that are ready to be processed and included in the block.

Queued transactions are transactions where the transaction nonce is not in sequence. The transaction nonce is an incrementing number for each transaction with the same From address.
* <https://ethereum.stackexchange.com/questions/3831/what-is-the-max-size-of-transactions-can-clients-like-geth-keep-in-txpool>


## IBFT Consensus

Updated quorum-examples/examples/7nodes/istanbul-start.sh

* Updated blockperiod from 5 to 1 second
* globalqueue to 20000

ARGS="--nodiscover --istanbul.blockperiod 5 --networkid $NETWORK_ID --syncmode full --mine --minerthreads 1 --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul $QUORUM_GETH_ARGS"
ARGS="--nodiscover --istanbul.blockperiod 1 --txpool.globalslots 20000 --txpool.globalqueue 20000 --networkid $NETWORK_ID --syncmode full --mine --minerthreads 1 --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul $QUORUM_GETH_ARGS"

## 10000

docker logs node3

INFO [06-12|07:27:55.302] Successfully wrote genesis state         database=lightchaindata                hash=85c72c…e7aec8
fatal error: concurrent map iteration and map write

goroutine 118 [running]:
runtime.throw(0x10c16ab, 0x26)
	/usr/local/go/src/runtime/panic.go:608 +0x72 fp=0xc02a85b958 sp=0xc02a85b928 pc=0x443b22
runtime.mapiternext(0xc02a85ba58)
	/usr/local/go/src/runtime/map.go:790 +0x525 fp=0xc02a85b9e0 sp=0xc02a85b958 pc=0x4262a5
github.com/ethereum/go-ethereum/core/state.(*StateDB).Logs(0xc0002efc70, 0xf6b8e0, 0xc0079dcb40, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/state/statedb.go:156 +0xe2 fp=0xc02a85bac8 sp=0xc02a85b9e0 pc=0x734382
github.com/ethereum/go-ethereum/miner.(*worker).resultLoop(0xc0001d8a80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:626 +0xbec fp=0xc02a85bfd8 sp=0xc02a85bac8 pc=0xb40b9c
runtime.goexit()
	/usr/local/go/src/runtime/asm_amd64.s:1333 +0x1 fp=0xc02a85bfe0 sp=0xc02a85bfd8 pc=0x474081
created by github.com/ethereum/go-ethereum/miner.newWorker
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:230 +0x595

goroutine 1 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/node.(*Node).Wait(0xc000182c80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/node/node.go:466 +0x7d
main.geth(0xc0002b7340, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/geth/main.go:279 +0x12c
github.com/ethereum/go-ethereum/vendor/gopkg.in/urfave/cli%2ev1.HandleAction(0xefb9c0, 0x1135f50, 0xc0002b7340, 0xc0002b7340, 0xc0004dff58)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/gopkg.in/urfave/cli.v1/app.go:490 +0xc8
github.com/ethereum/go-ethereum/vendor/gopkg.in/urfave/cli%2ev1.(*App).Run(0xc0002d2ea0, 0xc000030200, 0x20, 0x20, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/gopkg.in/urfave/cli.v1/app.go:264 +0x59d
main.main()
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/geth/main.go:260 +0x55

goroutine 19 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*nonrecursiveTree).dispatch(0xc0000ee180, 0xc0000ee0c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/tree_nonrecursive.go:36 +0xb6
created by github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.newNonrecursiveTree
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/tree_nonrecursive.go:29 +0xdc

goroutine 20 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*nonrecursiveTree).internal(0xc0000ee180, 0xc0000ee120)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/tree_nonrecursive.go:81 +0x7b
created by github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.newNonrecursiveTree
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/tree_nonrecursive.go:30 +0x108

goroutine 21 [syscall, 2 minutes]:
os/signal.signal_recv(0x0)
	/usr/local/go/src/runtime/sigqueue.go:139 +0x9c
os/signal.loop()
	/usr/local/go/src/os/signal/signal_unix.go:23 +0x22
created by os/signal.init.0
	/usr/local/go/src/os/signal/signal_unix.go:29 +0x41

goroutine 5 [chan receive]:
github.com/ethereum/go-ethereum/metrics.(*meterArbiter).tick(0x1e6e820)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/metrics/meter.go:289 +0x78
created by github.com/ethereum/go-ethereum/metrics.NewMeterForced
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/metrics/meter.go:70 +0xfd

goroutine 6 [select]:
github.com/ethereum/go-ethereum/consensus/ethash.(*Ethash).remote(0xc000240000, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/ethash/sealer.go:307 +0x526
created by github.com/ethereum/go-ethereum/consensus/ethash.New
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/ethash/ethash.go:496 +0x340

goroutine 50 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/patrickmn/go-cache.(*janitor).Run(0xc0002bc000, 0xc0002b0080)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/patrickmn/go-cache/cache.go:1037 +0x106
created by github.com/ethereum/go-ethereum/vendor/github.com/patrickmn/go-cache.runJanitor
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/patrickmn/go-cache/cache.go:1056 +0x70

goroutine 51 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 52 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 53 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 54 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 55 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 56 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 57 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 58 [chan receive]:
github.com/ethereum/go-ethereum/core.(*txSenderCacher).cache(0xc0002bc090)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:63 +0x9c
created by github.com/ethereum/go-ethereum/core.newTxSenderCacher
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_cacher.go:55 +0x9f

goroutine 9 [select]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).mpoolDrain(0xc0002d3520)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_state.go:101 +0xe7
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:143 +0x42e

goroutine 10 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).tCompaction(0xc0002d3520)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:834 +0x331
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:149 +0x58c

goroutine 8 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).compactionError(0xc0002d3520)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:90 +0xd3
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:142 +0x40c

goroutine 77 [chan receive]:
github.com/ethereum/go-ethereum/vendor/github.com/coreos/etcd/pkg/logutil.(*MergeLogger).outputLoop(0xc000569660)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/coreos/etcd/pkg/logutil/merge_logger.go:174 +0x39e
created by github.com/ethereum/go-ethereum/vendor/github.com/coreos/etcd/pkg/logutil.NewMergeLogger
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/coreos/etcd/pkg/logutil/merge_logger.go:92 +0x80

goroutine 36 [syscall, 2 minutes]:
syscall.Syscall6(0xe8, 0x6, 0xc00029ef6c, 0x1, 0xffffffffffffffff, 0x0, 0x0, 0x3, 0x0, 0x0)
	/usr/local/go/src/syscall/asm_linux_amd64.s:44 +0x5
github.com/ethereum/go-ethereum/vendor/golang.org/x/sys/unix.EpollWait(0x6, 0xc00029ef6c, 0x1, 0x1, 0xffffffffffffffff, 0x41cdc0, 0xc000570958, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/golang.org/x/sys/unix/zsyscall_linux_amd64.go:1529 +0x72
github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).loop(0xc00012c000, 0xc0001140c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:194 +0x8b
created by github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).lazyinit
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:134 +0x128

goroutine 85 [select, 2 minutes]:
github.com/ethereum/go-ethereum/accounts/keystore.(*watcher).loop(0xc00027f3a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/keystore/watch.go:94 +0x45a
created by github.com/ethereum/go-ethereum/accounts/keystore.(*watcher).start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/keystore/watch.go:52 +0x61

goroutine 37 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).send(0xc00012c000, 0xc0001140c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:254 +0xae
created by github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).lazyinit
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:137 +0x17b

goroutine 38 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).send(0xc00012c000, 0xc0001140c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:254 +0xae
created by github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify.(*inotify).lazyinit
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/rjeczalik/notify/watcher_inotify.go:137 +0x17b

goroutine 86 [select]:
github.com/ethereum/go-ethereum/accounts/keystore.(*KeyStore).updater(0xc00043c690)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/keystore/keystore.go:203 +0xc9
created by github.com/ethereum/go-ethereum/accounts/keystore.(*KeyStore).Subscribe
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/keystore/keystore.go:190 +0x123

goroutine 87 [sleep]:
time.Sleep(0x3b9aca00)
	/usr/local/go/src/runtime/time.go:105 +0x14f
github.com/ethereum/go-ethereum/accounts/usbwallet.(*Hub).updater(0xc000348a00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/usbwallet/hub.go:224 +0x3e
created by github.com/ethereum/go-ethereum/accounts/usbwallet.(*Hub).Subscribe
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/usbwallet/hub.go:213 +0x123

goroutine 88 [sleep]:
time.Sleep(0x3b9aca00)
	/usr/local/go/src/runtime/time.go:105 +0x14f
github.com/ethereum/go-ethereum/accounts/usbwallet.(*Hub).updater(0xc000348b40)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/usbwallet/hub.go:224 +0x3e
created by github.com/ethereum/go-ethereum/accounts/usbwallet.(*Hub).Subscribe
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/usbwallet/hub.go:213 +0x123

goroutine 89 [select, 2 minutes]:
github.com/ethereum/go-ethereum/accounts.(*Manager).update(0xc00027c0d0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/manager.go:95 +0x19c
created by github.com/ethereum/go-ethereum/accounts.NewManager
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/accounts/manager.go:68 +0x4fe

goroutine 90 [select]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util.(*BufferPool).drain(0xc000360620)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util/buffer_pool.go:206 +0x12a
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util.NewBufferPool
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util/buffer_pool.go:237 +0x177

goroutine 11 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).mCompaction(0xc0002d3520)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:762 +0x12e
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:150 +0x5ae

goroutine 12 [select]:
github.com/ethereum/go-ethereum/ethdb.(*LDBDatabase).meter(0xc0003233f0, 0xb2d05e00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/ethdb/database.go:337 +0x1503
created by github.com/ethereum/go-ethereum/ethdb.(*LDBDatabase).Meter
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/ethdb/database.go:174 +0x3fc

goroutine 13 [select, 2 minutes]:
github.com/ethereum/go-ethereum/core.(*ChainIndexer).updateLoop(0xc0002c4e00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/chain_indexer.go:307 +0x11f
created by github.com/ethereum/go-ethereum/core.NewChainIndexer
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/chain_indexer.go:116 +0x263

goroutine 14 [select]:
github.com/ethereum/go-ethereum/core.(*BlockChain).update(0xc0003e2000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/blockchain.go:1517 +0xfc
created by github.com/ethereum/go-ethereum/core.NewBlockChain
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/blockchain.go:204 +0x8ea

goroutine 15 [select]:
github.com/ethereum/go-ethereum/core.(*ChainIndexer).eventLoop(0xc0002c4e00, 0xc0002f2480, 0xc00009ccc0, 0x1311ca0, 0xc00027fc40)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/chain_indexer.go:207 +0x203
created by github.com/ethereum/go-ethereum/core.(*ChainIndexer).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/chain_indexer.go:148 +0xcf

goroutine 16 [select]:
github.com/ethereum/go-ethereum/core.(*TxPool).loop(0xc0001d88c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:296 +0x2c1
created by github.com/ethereum/go-ethereum/core.NewTxPool
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:268 +0x697

goroutine 114 [select]:
github.com/ethereum/go-ethereum/eth/downloader.(*Downloader).qosTuner(0xc0002d29c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/downloader.go:1629 +0x33e
created by github.com/ethereum/go-ethereum/eth/downloader.New
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/downloader.go:234 +0x3ec

goroutine 115 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth/downloader.(*Downloader).stateFetcher(0xc0002d29c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/statesync.go:76 +0x106
created by github.com/ethereum/go-ethereum/eth/downloader.New
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/downloader.go:235 +0x40e

goroutine 116 [runnable]:
github.com/ethereum/go-ethereum/core/types.MakeSigner(0xc00056c0a0, 0xc02a981a00, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/types/transaction_signing.go:46 +0x82
github.com/ethereum/go-ethereum/core.ApplyTransaction(0xc00056c0a0, 0xc0003e2000, 0xc006dd54d0, 0xc00857a2a8, 0xc0002efc70, 0xc0002efd40, 0xc0022b5440, 0xc02aeb85a0, 0xc0022b5610, 0x0, ...)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/state_processor.go:109 +0x22b
github.com/ethereum/go-ethereum/miner.(*worker).commitTransaction(0xc0001d8a80, 0xc02aeb85a0, 0x2161b065b5be6ce3, 0x3c4e478678763079, 0xa2091ff7c53a90de, 0x9d415fe6, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:754 +0x1a6
github.com/ethereum/go-ethereum/miner.(*worker).commitTransactions(0xc0001d8a80, 0xc00a339aa0, 0x2161b065b5be6ce3, 0x3c4e478678763079, 0xc53a90de, 0xc02b5fc350, 0xbf384867ac814936)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:831 +0x385
github.com/ethereum/go-ethereum/miner.(*worker).commitNewWork(0xc0001d8a80, 0xc02b5fc350, 0x0, 0x5d00aa1f)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:1005 +0xae1
github.com/ethereum/go-ethereum/miner.(*worker).mainLoop(0xc0001d8a80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:430 +0xba1
created by github.com/ethereum/go-ethereum/miner.newWorker
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:228 +0x544

goroutine 117 [select]:
github.com/ethereum/go-ethereum/miner.(*worker).newWorkLoop(0xc0001d8a80, 0xb2d05e00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:360 +0x28e
created by github.com/ethereum/go-ethereum/miner.newWorker
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:229 +0x573

goroutine 119 [select]:
github.com/ethereum/go-ethereum/miner.(*worker).taskLoop(0xc0001d8a80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:527 +0xf2
created by github.com/ethereum/go-ethereum/miner.newWorker
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/miner/worker.go:231 +0x5b7

goroutine 617 [IO wait]:
internal/poll.runtime_pollWait(0x7f55b398cc30, 0x72, 0xc0004b3b28)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc00396e118, 0x72, 0xffffffffffffff00, 0x1310280, 0x1afed80)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc00396e118, 0xc009364100, 0x20, 0x20)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Read(0xc00396e100, 0xc009364120, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:169 +0x179
net.(*netFD).Read(0xc00396e100, 0xc009364120, 0x20, 0x20, 0x8, 0x20, 0x7)
	/usr/local/go/src/net/fd_unix.go:202 +0x4f
net.(*conn).Read(0xc018a2c6b0, 0xc009364120, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/net.go:177 +0x68
io.ReadAtLeast(0x130eca0, 0xc018a2c6b0, 0xc009364120, 0x20, 0x20, 0x20, 0x1057ae0, 0xc00396e100, 0x130eca0)
	/usr/local/go/src/io/io.go:310 +0x88
io.ReadFull(0x130eca0, 0xc018a2c6b0, 0xc009364120, 0x20, 0x20, 0x20, 0xc00396e100, 0xbf38486f55d4d8a6)
	/usr/local/go/src/io/io.go:329 +0x58
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).ReadMsg(0xc004376cb0, 0xbf38486f55d4d8a6, 0x2d2e0c093f, 0x1e742a0, 0x0, 0x0, 0x1e742a0, 0x0, 0x12, 0xcc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:648 +0xfd
github.com/ethereum/go-ethereum/p2p.(*rlpx).ReadMsg(0xc009922ae0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:95 +0x127
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc01913f320, 0xc02bc3af00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:268 +0xa6
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 121 [select]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util.(*BufferPool).drain(0xc0003609a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util/buffer_pool.go:206 +0x12a
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util.NewBufferPool
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/util/buffer_pool.go:237 +0x177

goroutine 39 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).compactionError(0xc0000e8d00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:90 +0xd3
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:142 +0x40c

goroutine 40 [select]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).mpoolDrain(0xc0000e8d00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_state.go:101 +0xe7
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:143 +0x42e

goroutine 41 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).tCompaction(0xc0000e8d00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:834 +0x331
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:149 +0x58c

goroutine 42 [select, 2 minutes]:
github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.(*DB).mCompaction(0xc0000e8d00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db_compaction.go:762 +0x12e
created by github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb.openDB
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/syndtr/goleveldb/leveldb/db.go:150 +0x5ae

goroutine 173 [select, 2 minutes]:
net.(*pipe).read(0xc000295f00, 0xc018a4e200, 0x200, 0x200, 0xc0004b2a78, 0x45ae07, 0x200)
	/usr/local/go/src/net/pipe.go:164 +0x1a4
net.(*pipe).Read(0xc000295f00, 0xc018a4e200, 0x200, 0x200, 0x0, 0x200, 0x7f55b38e1b48)
	/usr/local/go/src/net/pipe.go:147 +0x4d
encoding/json.(*Decoder).refill(0xc018ae2160, 0x6, 0xc0004b2ba0)
	/usr/local/go/src/encoding/json/stream.go:159 +0x102
encoding/json.(*Decoder).readValue(0xc018ae2160, 0x0, 0x0, 0x60)
	/usr/local/go/src/encoding/json/stream.go:134 +0x222
encoding/json.(*Decoder).Decode(0xc018ae2160, 0xf48520, 0xc000568240, 0xf23301, 0xc000568240)
	/usr/local/go/src/encoding/json/stream.go:63 +0x78
encoding/json.(*Decoder).Decode-fm(0xf48520, 0xc000568240, 0xc0002de428, 0x462e53)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/json.go:119 +0x3e
github.com/ethereum/go-ethereum/rpc.(*jsonCodec).ReadRequestHeaders(0xc0002de410, 0x0, 0x0, 0x0, 0x423200, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/json.go:144 +0xbe
github.com/ethereum/go-ethereum/rpc.(*Server).readRequest(0xc0002796b0, 0x1320a00, 0xc0002de410, 0x1e9c801, 0xfb4320, 0xc000278390, 0x131a8a0, 0xc000278450, 0xc018ac1fc0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/server.go:379 +0x5f
github.com/ethereum/go-ethereum/rpc.(*Server).serveRequest(0xc0002796b0, 0x131a820, 0xc000034060, 0x1320a00, 0xc0002de410, 0x0, 0x3, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/server.go:160 +0x2c1
github.com/ethereum/go-ethereum/rpc.(*Server).ServeCodec(0xc0002796b0, 0x1320a00, 0xc0002de410, 0x3)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/server.go:216 +0x94
created by github.com/ethereum/go-ethereum/rpc.DialInProc.func1
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/inproc.go:29 +0xbd

goroutine 44 [IO wait, 1 minutes]:
internal/poll.runtime_pollWait(0x7f55b398cea0, 0x72, 0x0)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc000294198, 0x72, 0xc0002a2900, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc000294198, 0xffffffffffffff00, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Accept(0xc000294180, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:384 +0x1a0
net.(*netFD).accept(0xc000294180, 0xc02b08c9e0, 0xc0003b2730, 0xc0002a0538)
	/usr/local/go/src/net/fd_unix.go:238 +0x42
net.(*TCPListener).accept(0xc000274608, 0x41ce8b, 0xc0002a04e0, 0x0)
	/usr/local/go/src/net/tcpsock_posix.go:139 +0x2e
net.(*TCPListener).Accept(0xc000274608, 0x0, 0xc000182f00, 0x131f9e0, 0xc0189dd6c0)
	/usr/local/go/src/net/tcpsock.go:260 +0x47
github.com/ethereum/go-ethereum/p2p.(*Server).listenLoop(0xc000182f00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:849 +0x22b
created by github.com/ethereum/go-ethereum/p2p.(*Server).setupListening
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:588 +0x190

goroutine 45 [select, 2 minutes]:
github.com/ethereum/go-ethereum/p2p/nat.Map(0x131b060, 0xc0002b16c0, 0xc0002f5f80, 0x1098ec9, 0x3, 0x5208, 0x5208, 0x10a26b7, 0xc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/nat/nat.go:114 +0x45d
github.com/ethereum/go-ethereum/p2p.(*Server).setupListening.func1(0xc000182f00, 0xc000279410)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:594 +0x8b
created by github.com/ethereum/go-ethereum/p2p.(*Server).setupListening
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:593 +0x224

goroutine 46 [select]:
github.com/ethereum/go-ethereum/p2p.(*Server).run(0xc000182f00, 0x131af60, 0xc0002f00c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:661 +0x6b5
created by github.com/ethereum/go-ethereum/p2p.(*Server).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:465 +0x4e4

goroutine 47 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 48 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 49 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 130 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 131 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 132 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 133 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 134 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 135 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 136 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 137 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 138 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 139 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 140 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 141 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 142 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers.func1(0xc000360700, 0x1000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:56 +0x32a
created by github.com/ethereum/go-ethereum/eth.(*Ethereum).startBloomHandlers
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/bloombits.go:54 +0x52

goroutine 143 [select]:
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).txBroadcastLoop(0xc00027c4e0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:815 +0x10f
created by github.com/ethereum/go-ethereum/eth.(*ProtocolManager).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:224 +0xbe

goroutine 144 [runnable]:
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).minedBroadcastLoop(0xc00027c4e0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:805 +0x59
created by github.com/ethereum/go-ethereum/eth.(*ProtocolManager).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:229 +0x19d

goroutine 145 [select]:
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).syncer(0xc00027c4e0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/sync.go:145 +0x18b
created by github.com/ethereum/go-ethereum/eth.(*ProtocolManager).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:238 +0xf4

goroutine 146 [select, 2 minutes]:
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).txsyncLoop(0xc00027c4e0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/sync.go:109 +0x21a
created by github.com/ethereum/go-ethereum/eth.(*ProtocolManager).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:239 +0x116

goroutine 147 [select, 1 minutes]:
github.com/ethereum/go-ethereum/eth/downloader.(*PublicDownloaderAPI).eventLoop(0xc000355340)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/api.go:63 +0x2b5
created by github.com/ethereum/go-ethereum/eth/downloader.NewPublicDownloaderAPI
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/downloader/api.go:49 +0xca

goroutine 148 [select]:
github.com/ethereum/go-ethereum/eth/filters.(*EventSystem).eventLoop(0xc0002d4820)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/filters/filter_system.go:464 +0x4d3
created by github.com/ethereum/go-ethereum/eth/filters.NewEventSystem
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/filters/filter_system.go:146 +0x3aa

goroutine 149 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/eth/filters.(*PublicFilterAPI).timeoutLoop(0xc0002dea00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/filters/api.go:83 +0x125
created by github.com/ethereum/go-ethereum/eth/filters.NewPublicFilterAPI
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/filters/api.go:73 +0x149

goroutine 174 [select, 2 minutes]:
github.com/ethereum/go-ethereum/rpc.(*Client).dispatch(0xc01899e600, 0x131fb00, 0xc000295f80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/client.go:507 +0x280
created by github.com/ethereum/go-ethereum/rpc.newClient
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/client.go:206 +0x2cb

goroutine 94 [runnable]:
internal/poll.runtime_pollWait(0x7f55b398ca90, 0x72, 0xc0004aeb28)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc0189b0418, 0x72, 0xffffffffffffff00, 0x1310280, 0x1afed80)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc0189b0418, 0xc009365600, 0x20, 0x20)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Read(0xc0189b0400, 0xc0093656a0, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:169 +0x179
net.(*netFD).Read(0xc0189b0400, 0xc0093656a0, 0x20, 0x20, 0xc, 0x20, 0xb)
	/usr/local/go/src/net/fd_unix.go:202 +0x4f
net.(*conn).Read(0xc018a2c000, 0xc0093656a0, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/net.go:177 +0x68
io.ReadAtLeast(0x130eca0, 0xc018a2c000, 0xc0093656a0, 0x20, 0x20, 0x20, 0x1057ae0, 0xc0189b0400, 0x130eca0)
	/usr/local/go/src/io/io.go:310 +0x88
io.ReadFull(0x130eca0, 0xc018a2c000, 0xc0093656a0, 0x20, 0x20, 0x20, 0xc0189b0400, 0xbf38486f5610dd8f)
	/usr/local/go/src/io/io.go:329 +0x58
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).ReadMsg(0xc0002f6230, 0xbf38486f5610dd8f, 0x2d2e480e1a, 0x1e742a0, 0x0, 0x0, 0x1e742a0, 0x0, 0x12, 0xcc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:648 +0xfd
github.com/ethereum/go-ethereum/p2p.(*rlpx).ReadMsg(0xc018a2a090, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:95 +0x127
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc01899a720, 0xc01899a900)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:268 +0xa6
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 204 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).handle(0xc0189bd5c0, 0x12, 0xcc, 0x130d260, 0xc029981230, 0xbf384867d6589084, 0x26326c1529, 0x1e742a0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:301 +0x14e
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc0189bd5c0, 0xc0189bd7a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:274 +0x191
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 153 [runnable]:
internal/poll.runtime_pollWait(0x7f55b398cdd0, 0x72, 0xc00006bb28)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc00030fb98, 0x72, 0xffffffffffffff00, 0x1310280, 0x1afed80)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc00030fb98, 0xc0045d3b00, 0x20, 0x20)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Read(0xc00030fb80, 0xc0045d3b20, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:169 +0x179
net.(*netFD).Read(0xc00030fb80, 0xc0045d3b20, 0x20, 0x20, 0x5, 0x20, 0x4)
	/usr/local/go/src/net/fd_unix.go:202 +0x4f
net.(*conn).Read(0xc0002ba060, 0xc0045d3b20, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/net.go:177 +0x68
io.ReadAtLeast(0x130eca0, 0xc0002ba060, 0xc0045d3b20, 0x20, 0x20, 0x20, 0x1057ae0, 0xc00030fb00, 0x130eca0)
	/usr/local/go/src/io/io.go:310 +0x88
io.ReadFull(0x130eca0, 0xc0002ba060, 0xc0045d3b20, 0x20, 0x20, 0x20, 0xc00030fb80, 0xbf38486f54f9c388)
	/usr/local/go/src/io/io.go:329 +0x58
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).ReadMsg(0xc0003f02a0, 0xbf38486f54f9c388, 0x2d2d30f411, 0x1e742a0, 0x0, 0x0, 0x1e742a0, 0x0, 0x12, 0xcc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:648 +0xfd
github.com/ethereum/go-ethereum/p2p.(*rlpx).ReadMsg(0xc01899c090, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:95 +0x127
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc018993da0, 0xc018993f80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:268 +0xa6
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 93 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc01899a720, 0xea5960, 0xc01899a840, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc01899a720)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 175 [select, 2 minutes]:
net.(*pipe).read(0xc000295f80, 0xc018a1e600, 0x200, 0x200, 0xc0003b2d90, 0x45ae07, 0x200)
	/usr/local/go/src/net/pipe.go:164 +0x1a4
net.(*pipe).Read(0xc000295f80, 0xc018a1e600, 0x200, 0x200, 0x0, 0x200, 0x8)
	/usr/local/go/src/net/pipe.go:147 +0x4d
encoding/json.(*Decoder).refill(0xc018ae22c0, 0x203000, 0x6)
	/usr/local/go/src/encoding/json/stream.go:159 +0x102
encoding/json.(*Decoder).readValue(0xc018ae22c0, 0x0, 0x0, 0xec0580)
	/usr/local/go/src/encoding/json/stream.go:134 +0x222
encoding/json.(*Decoder).Decode(0xc018ae22c0, 0xf48520, 0xc0002aed00, 0x0, 0xc0003b2f10)
	/usr/local/go/src/encoding/json/stream.go:63 +0x78
github.com/ethereum/go-ethereum/rpc.(*Client).read.func1(0xfab440, 0xc0002aed20, 0xc000295f80, 0x7f55b3ad5810, 0xc000295f80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/client.go:648 +0x79
github.com/ethereum/go-ethereum/rpc.(*Client).read(0xc01899e600, 0x131fb00, 0xc000295f80, 0xc000000008, 0x8)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/client.go:661 +0x13f
created by github.com/ethereum/go-ethereum/rpc.(*Client).dispatch
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/client.go:482 +0x71

goroutine 1048 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc00707bbc0, 0xea5960, 0xc00707bce0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc00707bbc0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 150 [select]:
github.com/ethereum/go-ethereum/eth/fetcher.(*Fetcher).loop(0xc00027c5b0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/fetcher/fetcher.go:316 +0x631
created by github.com/ethereum/go-ethereum/eth/fetcher.(*Fetcher).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/fetcher/fetcher.go:178 +0x3f

goroutine 178 [IO wait, 2 minutes]:
internal/poll.runtime_pollWait(0x7f55b398c9c0, 0x72, 0x0)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc0004bd798, 0x72, 0xc0000f0300, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc0004bd798, 0xffffffffffffff00, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Accept(0xc0004bd780, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:384 +0x1a0
net.(*netFD).accept(0xc0004bd780, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/fd_unix.go:238 +0x42
net.(*UnixListener).accept(0xc0189c8000, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/unixsock_posix.go:162 +0x32
net.(*UnixListener).Accept(0xc0189c8000, 0x0, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/unixsock.go:257 +0x47
github.com/ethereum/go-ethereum/rpc.(*Server).ServeListener(0xc00021b470, 0x1318ce0, 0xc0189c8000, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/ipc.go:30 +0x49
created by github.com/ethereum/go-ethereum/rpc.StartIPCEndpoint
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/endpoints.go:100 +0x298

goroutine 203 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc0189bd5c0, 0xea5960, 0xc0189bd6e0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc0189bd5c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 205 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc0189bd5c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:206 +0x11d

goroutine 206 [runnable]:
fmt.(*pp).doPrintf(0xc00417cb40, 0x10ac821, 0x15, 0xc02b072c00, 0x1, 0x1)
	/usr/local/go/src/fmt/print.go:951 +0x1258
fmt.Sprintf(0x10ac821, 0x15, 0xc02b072c00, 0x1, 0x1, 0xc02b072b28, 0x20)
	/usr/local/go/src/fmt/print.go:203 +0x66
fmt.Errorf(0x10ac821, 0x15, 0xc02b072c00, 0x1, 0x1, 0xc007569c20, 0xc02b0729c0)
	/usr/local/go/src/fmt/print.go:212 +0x57
github.com/ethereum/go-ethereum/core.(*TxPool).add(0xc0001d88c0, 0xc02b83cc60, 0x0, 0xc0043a1c30, 0x1, 0x1)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:647 +0x14ee
github.com/ethereum/go-ethereum/core.(*TxPool).addTxsLocked(0xc0001d88c0, 0xc0030c12a0, 0x1, 0x4, 0xc02ac39d00, 0xc02ac39d20, 0xc02ac39d20, 0xc02b0730b0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:857 +0x12e
github.com/ethereum/go-ethereum/core.(*TxPool).addTxs(0xc0001d88c0, 0xc0030c12a0, 0x1, 0x4, 0x5fa5f72887bafb00, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:845 +0xaf
github.com/ethereum/go-ethereum/core.(*TxPool).AddRemotes(0xc0001d88c0, 0xc0030c12a0, 0x1, 0x4, 0xb194876158f7514e, 0x26324f75d7, 0x1e742a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:819 +0x4e
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc018ae6180, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:725 +0x4bf
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc018ae6180, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc0189bd5c0, 0x1311fa0, 0xc018ade510, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc018ade510, 0xc0189bd5c0, 0x1311fa0, 0xc018ade510)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 212 [runnable]:
syscall.Syscall(0x1, 0x1c, 0xc00b02ec60, 0x89, 0x89, 0x89, 0x0)
	/usr/local/go/src/syscall/asm_linux_amd64.s:18 +0x5
syscall.write(0x1c, 0xc00b02ec60, 0x89, 0x89, 0xc000249600, 0x0, 0x0)
	/usr/local/go/src/syscall/zsyscall_linux_amd64.go:1005 +0x5a
syscall.Write(0x1c, 0xc00b02ec60, 0x89, 0x89, 0xf40c41fe1adece15, 0x968c857d8a71ae1b, 0x786f4bd20c58b66e)
	/usr/local/go/src/syscall/syscall_unix.go:191 +0x49
internal/poll.(*FD).Write(0xc01899e580, 0xc00b02ec60, 0x89, 0x89, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:268 +0x165
net.(*netFD).Write(0xc01899e580, 0xc00b02ec60, 0x89, 0x89, 0x10e, 0xc018a1ea51, 0x1af)
	/usr/local/go/src/net/fd_unix.go:220 +0x4f
net.(*conn).Write(0xc0002ba1c0, 0xc00b02ec60, 0x89, 0x89, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/net.go:189 +0x68
io.(*multiWriter).Write(0xc00c20f200, 0xc00b02ec60, 0x89, 0x89, 0xc02a608240, 0x89, 0x10e)
	/usr/local/go/src/io/multi.go:60 +0x87
crypto/cipher.StreamWriter.Write(0x130d300, 0xc018a06280, 0x130eb80, 0xc00c20f200, 0x0, 0x0, 0xc02a608240, 0x89, 0x10e, 0xf577a0, ...)
	/usr/local/go/src/crypto/cipher/io.go:39 +0xdd
bytes.(*Reader).WriteTo(0xc007da9620, 0x130f3c0, 0xc029981260, 0x7f55b2941060, 0xc007da9620, 0x1)
	/usr/local/go/src/bytes/reader.go:140 +0xa1
io.copyBuffer(0x130f3c0, 0xc029981260, 0x130d260, 0xc007da9620, 0x0, 0x0, 0x0, 0xc029981260, 0xc029981260, 0xff2a00)
	/usr/local/go/src/io/io.go:384 +0x352
io.Copy(0x130f3c0, 0xc029981260, 0x130d260, 0xc007da9620, 0x0, 0x0, 0xc009de60d0)
	/usr/local/go/src/io/io.go:364 +0x5a
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).WriteMsg(0xc0001d6460, 0x12, 0x89, 0x130d260, 0xc007da9620, 0x0, 0x0, 0x0, 0xc000907c00, 0x203001)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:628 +0x582
github.com/ethereum/go-ethereum/p2p.(*rlpx).WriteMsg(0xc01899c480, 0x12, 0xcc, 0x130dc60, 0xc007da95f0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:102 +0xfc
github.com/ethereum/go-ethereum/p2p.(*protoRW).WriteMsg(0xc018ade510, 0x12, 0xcc, 0x130dc60, 0xc007da95f0, 0x0, 0x0, 0x0, 0xc018ec8201, 0xc00c20f1a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:402 +0x160
github.com/ethereum/go-ethereum/p2p.Send(0x7f55b2941040, 0xc018ade510, 0x2, 0xf6bce0, 0xc00c20f1a0, 0x1907988, 0xc02ab0d980)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/message.go:97 +0xbb
github.com/ethereum/go-ethereum/eth.(*peer).SendTransactions(0xc018ae6180, 0xc02b47afb0, 0x1, 0x1, 0x1, 0x2)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:208 +0x18b
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc018ae6180)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:118 +0x9c0
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 152 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc018993da0, 0xea5960, 0xc018993ec0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc018993da0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 154 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc018993da0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:206 +0x11d

goroutine 155 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).ReadMsg(0xc000350750, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:415 +0x101
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc0002f03c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:344 +0xa6
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc0002f03c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc018993da0, 0x1311fa0, 0xc000350750, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc000350750, 0xc018993da0, 0x1311fa0, 0xc000350750)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 95 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc01899a720)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:206 +0x11d

goroutine 96 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).ReadMsg(0xc018b7c480, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:415 +0x101
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc0189f00c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:344 +0xa6
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc0189f00c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc01899a720, 0x1311fa0, 0xc018b7c480, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc018b7c480, 0xc01899a720, 0x1311fa0, 0xc018b7c480)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 22 [chan receive, 2 minutes]:
main.startNode.func1(0xc000182c80, 0xc000245740)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/geth/main.go:322 +0x1b6
created by main.startNode
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/geth/main.go:307 +0x2ac

goroutine 158 [select]:
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc0002f03c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:116 +0x14b
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 618 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc01913f320)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:206 +0x11d

goroutine 227 [select]:
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc0189f00c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:116 +0x14b
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 307 [select]:
github.com/ethereum/go-ethereum/consensus/istanbul/core.(*core).handleEvents(0xc0002b8b40)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/istanbul/core/handler.go:83 +0x172
created by github.com/ethereum/go-ethereum/consensus/istanbul/core.(*core).Start
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/istanbul/core/handler.go:32 +0x67

goroutine 179 [IO wait]:
internal/poll.runtime_pollWait(0x7f55b398c680, 0x72, 0x0)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc0189bfa98, 0x72, 0xc0000f0500, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc0189bfa98, 0xffffffffffffff00, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Accept(0xc0189bfa80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:384 +0x1a0
net.(*netFD).accept(0xc0189bfa80, 0xf70d5b7a, 0xcd485d18c711b247, 0x463948)
	/usr/local/go/src/net/fd_unix.go:238 +0x42
net.(*TCPListener).accept(0xc0189dc490, 0x5d00aa1d, 0xc018af9eb8, 0x4a5f46)
	/usr/local/go/src/net/tcpsock_posix.go:139 +0x2e
net.(*TCPListener).Accept(0xc0189dc490, 0xc018af9f08, 0x18, 0xc0189ca180, 0x84d695)
	/usr/local/go/src/net/tcpsock.go:260 +0x47
net/http.(*Server).Serve(0xc0189e9c70, 0x1318ca0, 0xc0189dc490, 0x0, 0x0)
	/usr/local/go/src/net/http/server.go:2826 +0x22f
created by github.com/ethereum/go-ethereum/rpc.StartHTTPEndpoint
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/rpc/endpoints.go:50 +0x3c0

goroutine 180 [chan receive, 2 minutes]:
github.com/ethereum/go-ethereum/cmd/utils.StartNode.func1(0xc000182c80)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/utils/cmd.go:74 +0xe3
created by github.com/ethereum/go-ethereum/cmd/utils.StartNode
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/cmd/utils/cmd.go:70 +0xad

goroutine 557 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).WriteMsg(0xc00833db90, 0x12, 0xcc, 0x130dc60, 0xc0292db1d0, 0x0, 0x0, 0x0, 0xc0091cba01, 0xc0299b1b40)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:400 +0xcd
github.com/ethereum/go-ethereum/p2p.Send(0x7f55b2941040, 0xc00833db90, 0x2, 0xf6bce0, 0xc0299b1b40, 0x1907988, 0xc00457c000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/message.go:97 +0xbb
github.com/ethereum/go-ethereum/eth.(*peer).SendTransactions(0xc0060f2cc0, 0xc018aa13c0, 0x1, 0x1, 0x1, 0x2)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:208 +0x18b
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc0060f2cc0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:118 +0x9c0
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 9664 [semacquire]:
sync.runtime_SemacquireMutex(0xc0001d8a0c, 0x48c100)
	/usr/local/go/src/runtime/sema.go:71 +0x3d
sync.(*Mutex).Lock(0xc0001d8a08)
	/usr/local/go/src/sync/mutex.go:134 +0xff
sync.(*RWMutex).Lock(0xc0001d8a08)
	/usr/local/go/src/sync/rwmutex.go:93 +0x2d
github.com/ethereum/go-ethereum/core.(*TxPool).addTxs(0xc0001d88c0, 0xc00bbd20a0, 0x1, 0x4, 0x90efbf247858ab00, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:842 +0x54
github.com/ethereum/go-ethereum/core.(*TxPool).AddRemotes(0xc0001d88c0, 0xc00bbd20a0, 0x1, 0x4, 0x20e035ffe7de4610, 0x26325782ac, 0x1e742a0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/core/tx_pool.go:819 +0x4e
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc02b4406c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:725 +0x4bf
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc02b4406c0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc018fd4a20, 0x1311fa0, 0xc007c94cf0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc007c94cf0, 0xc018fd4a20, 0x1311fa0, 0xc007c94cf0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 619 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).ReadMsg(0xc00833db90, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:415 +0x101
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc0060f2cc0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:344 +0xa6
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc0060f2cc0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc01913f320, 0x1311fa0, 0xc00833db90, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc00833db90, 0xc01913f320, 0x1311fa0, 0xc00833db90)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 9684 [select]:
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc02b4406c0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:116 +0x14b
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 1051 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).ReadMsg(0xc003ef0c60, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:415 +0x101
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handleMsg(0xc00027c4e0, 0xc02b792600, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:344 +0xa6
github.com/ethereum/go-ethereum/eth.(*ProtocolManager).handle(0xc00027c4e0, 0xc02b792600, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:333 +0x7eb
github.com/ethereum/go-ethereum/eth.NewProtocolManager.func1(0xc00707bbc0, 0x1311fa0, 0xc003ef0c60, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/handler.go:157 +0x1b8
github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols.func1(0xc003ef0c60, 0xc00707bbc0, 0x1311fa0, 0xc003ef0c60)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:361 +0x66
created by github.com/ethereum/go-ethereum/p2p.(*Peer).startProtocols
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:360 +0x1fb

goroutine 504 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc01913f320, 0xea5960, 0xc02bc3ae40, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc01913f320)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 9662 [IO wait]:
internal/poll.runtime_pollWait(0x7f55b398c8f0, 0x72, 0xc0009ccb28)
	/usr/local/go/src/runtime/netpoll.go:173 +0x66
internal/poll.(*pollDesc).wait(0xc006e38198, 0x72, 0xffffffffffffff00, 0x1310280, 0x1afed80)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:85 +0x9a
internal/poll.(*pollDesc).waitRead(0xc006e38198, 0xc009365800, 0x20, 0x20)
	/usr/local/go/src/internal/poll/fd_poll_runtime.go:90 +0x3d
internal/poll.(*FD).Read(0xc006e38180, 0xc009365800, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/internal/poll/fd_unix.go:169 +0x179
net.(*netFD).Read(0xc006e38180, 0xc009365800, 0x20, 0x20, 0xc, 0x20, 0xb)
	/usr/local/go/src/net/fd_unix.go:202 +0x4f
net.(*conn).Read(0xc0189dda98, 0xc009365800, 0x20, 0x20, 0x0, 0x0, 0x0)
	/usr/local/go/src/net/net.go:177 +0x68
io.ReadAtLeast(0x130eca0, 0xc0189dda98, 0xc009365800, 0x20, 0x20, 0x20, 0x1057ae0, 0xc006e38100, 0x130eca0)
	/usr/local/go/src/io/io.go:310 +0x88
io.ReadFull(0x130eca0, 0xc0189dda98, 0xc009365800, 0x20, 0x20, 0x20, 0xc006e38180, 0xbf38486f56440f3d)
	/usr/local/go/src/io/io.go:329 +0x58
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).ReadMsg(0xc0001d64d0, 0xbf38486f56440f3d, 0x2d2e7b3fc6, 0x1e742a0, 0x0, 0x0, 0x1e742a0, 0x0, 0x12, 0xcc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:648 +0xfd
github.com/ethereum/go-ethereum/p2p.(*rlpx).ReadMsg(0xc00a85d590, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:95 +0x127
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc018fd4a20, 0xc018fd4c00)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:268 +0xa6
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 18794 [select]:
github.com/ethereum/go-ethereum/p2p.(*protoRW).WriteMsg(0xc00833db90, 0x21, 0xc8, 0x130dc60, 0xc02a9b59b0, 0x0, 0x0, 0x0, 0xc00a7b3700, 0x7f55b2941040)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:400 +0xcd
github.com/ethereum/go-ethereum/p2p.Send(0x7f55b2941040, 0xc00833db90, 0x11, 0xecba00, 0xc007a77960, 0x12, 0xf6b960)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/message.go:97 +0xbb
github.com/ethereum/go-ethereum/eth.(*peer).Send(0xc0060f2cc0, 0x11, 0xecba00, 0xc007a77960, 0xc0079dcb40, 0xc00225a000)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:199 +0x7e
created by github.com/ethereum/go-ethereum/consensus/istanbul/backend.(*backend).Gossip
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/istanbul/backend/backend.go:160 +0x62a

goroutine 1000 [select]:
github.com/ethereum/go-ethereum/eth.(*peer).broadcast(0xc02b792600)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:116 +0x14b
created by github.com/ethereum/go-ethereum/eth.(*peerSet).Register
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:434 +0x122

goroutine 1050 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc00707bbc0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:206 +0x11d

goroutine 1049 [runnable]:
github.com/ethereum/go-ethereum/vendor/github.com/golang/snappy.DecodedLen(0xc000ef0c00, 0x89, 0x600, 0x89, 0x600, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/vendor/github.com/golang/snappy/decode.go:25 +0x6c
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).ReadMsg(0xc02b1261c0, 0xbf38486f55868583, 0x2d2dbdb614, 0x1e742a0, 0x0, 0x0, 0x1e742a0, 0x0, 0x12, 0xcc)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:698 +0x6dc
github.com/ethereum/go-ethereum/p2p.(*rlpx).ReadMsg(0xc00021b1a0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:95 +0x127
github.com/ethereum/go-ethereum/p2p.(*Peer).readLoop(0xc00707bbc0, 0xc00707bda0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:268 +0xa6
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:205 +0xf8

goroutine 18804 [runnable]:
github.com/ethereum/go-ethereum/p2p.(*rlpxFrameRW).WriteMsg(0xc004376cb0, 0x21, 0xcc, 0x130d260, 0xc0292db4a0, 0x0, 0x0, 0x0, 0xc008545ed0, 0x203001)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:607 +0x1b7
github.com/ethereum/go-ethereum/p2p.(*rlpx).WriteMsg(0xc009922ae0, 0x21, 0xc8, 0x130dc60, 0xc007d990b0, 0x0, 0x0, 0x0, 0x0, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/rlpx.go:102 +0xfc
github.com/ethereum/go-ethereum/p2p.(*protoRW).WriteMsg(0xc00833db90, 0x21, 0xc8, 0x130dc60, 0xc007d990b0, 0x0, 0x0, 0x0, 0x1327300, 0x7f55b2941040)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:402 +0x160
github.com/ethereum/go-ethereum/p2p.Send(0x7f55b2941040, 0xc00833db90, 0x11, 0xecba00, 0xc00b6fcac0, 0xc0083b2200, 0x44be49)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/message.go:97 +0xbb
github.com/ethereum/go-ethereum/eth.(*peer).Send(0xc0060f2cc0, 0x11, 0xecba00, 0xc00b6fcac0, 0xc000459cd8, 0x3)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/eth/peer.go:199 +0x7e
created by github.com/ethereum/go-ethereum/consensus/istanbul/backend.(*backend).Gossip
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/consensus/istanbul/backend/backend.go:160 +0x62a

goroutine 9660 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).run(0xc018fd4a20, 0xea5960, 0xc018fd4b40, 0x0)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:215 +0x279
github.com/ethereum/go-ethereum/p2p.(*Server).runPeer(0xc000182f00, 0xc018fd4a20)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:1040 +0x150
created by github.com/ethereum/go-ethereum/p2p.(*Server).run
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/server.go:738 +0x13f9

goroutine 9663 [select]:
github.com/ethereum/go-ethereum/p2p.(*Peer).pingLoop(0xc018fd4a20)
	/go-ethereum/build/_workspace/src/github.com/ethereum/go-ethereum/p2p/peer.go:252 +0x143
created by github.com/ethereum/go-ethereum/p2p.(*Peer).run

### https://github.com/ethereum/go-ethereum/issues/16449 Fiexd in
### https://github.com/ethereum/go-ethereum/pull/17173
### https://github.com/jpmorganchase/quorum/issues/653 : Fixed for clique consensus

## docker logs node1

INFO [06-12|07:13:31.331] Successfully wrote genesis state         database=lightchaindata                hash=85c72c…e7aec8
panic: revision id 380 cannot be reverted
