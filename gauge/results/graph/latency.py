import matplotlib.pyplot as plt

# RAFT data
x = [10000, 100000]
y0 = [0.875, 0.866] # Register Binding
y1 = [0.012, 0.102] # Query Binding

# IBFT data
y4 = [1.783, 2.061] # Register Binding
y5 = [0.014, 0.126] # Query Binding

# Requirements
y8 = [3, 3] # Pratical latency requirements, 3 sec
plt.plot(x, y8, label="Pratical Requirements", linestyle='dashed')

# RAFT
plt.plot(x, y0, label="RAFT Register Binding")
plt.plot(x, y1, label="RAFT Query Binding")

# IBFT
plt.plot(x, y4, label="IBFT Register Binding")
plt.plot(x, y5, label="IBFT Query Binding")

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Latency")

plt.legend(bbox_to_anchor=(.05, 0.93), loc=2, ncol=2)

plt.show()