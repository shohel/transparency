import matplotlib.pyplot as plt

x = [1000, 10000, 100000]
y0 = [5.007, 7.125, 3.362] # Add Binding
y1 = [1.470, 5.930, 0.823] # Query Binding
y2 = [4.889, 3.523, 1.489] # Sign Binding
y3 = [1.490, 1.978, 0.689] # Query Sign Binding

plt.plot(x, y0, label="IBFT Add Binding", marker='o')
plt.plot(x, y1, label="IBFT Query Binding", marker='o')
plt.plot(x, y2, label="IBFT Sign Binding", marker='o')
plt.plot(x, y3, label="IBFT Query Sign Binding", marker='o')

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Latency")

plt.legend()

plt.show()