import matplotlib.pyplot as plt

x = [1000, 10000, 100000] # multiple of 1000
y0 = [259, 239, 150] # AB_TPS_average
y1 = [1248, 712, 227] # QB_TPS_average
y2 = [178, 148, 80] # SB_TPS_average
y3 = [1396, 803, 201] # SQB_TPS_average

plt.plot(x, y0, label="RAFT Add Binding", marker='o')
plt.plot(x, y1, label="RAFT Query Binding", marker='o')
plt.plot(x, y2, label="RAFT Sign Binding", marker='o')
plt.plot(x, y3, label="RAFT Query Sign Binding", marker='o')

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Transaction Per Second")

plt.legend()

plt.show()