import matplotlib.pyplot as plt

x = [1000, 10000, 100000]
y0 = [3.785, 9.447, 0.806] # Add Binding
y1 = [1.560, 6.357, 0.594] # Query Binding
y2 = [3.849, 14.423, 0.850] # Sign Binding
y3 = [1.498, 5.786, 0.219] # Query Sign Binding

y4 = [5.007, 7.125, 3.362] # Add Binding
y5 = [1.470, 5.930, 0.823] # Query Binding
y6 = [4.889, 3.523, 1.489] # Sign Binding
y7 = [1.490, 1.978, 0.689] # Query Sign Binding

# RAFT
plt.plot(x, y0, label="RAFT Add Binding", marker='o')
plt.plot(x, y1, label="RAFT Query Binding", marker='o')
plt.plot(x, y2, label="RAFT Sign Binding", marker='o')
plt.plot(x, y3, label="RAFT Query Sign Binding", marker='o')

# IBFT
plt.plot(x, y4, label="IBFT Add Binding", marker='o')
plt.plot(x, y5, label="IBFT Query Binding", marker='o')
plt.plot(x, y6, label="IBFT Sign Binding", marker='o')
plt.plot(x, y7, label="IBFT Query Sign Binding", marker='o')

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Latency")

plt.legend()

plt.show()