import matplotlib.pyplot as plt

x = [10000, 100000] # multiple of 1000

# RAFT
y0 = [80, 80] # AB_TPS_average
y1 = [252, 251] # QB_TPS_average
y2 = [80, 80] # SB_TPS_average
y3 = [253, 249] # SQB_TPS_average

# IBFT
y4 = [80, 80] # AB_TPS_average
y5 = [253, 250] # QB_TPS_average
y6 = [80, 80] # SB_TPS_average
y7 = [253, 251] # SQB_TPS_averag

#plt.plot(x, y0, label="RAFT Add Binding")
plt.plot(x, y0, label="RAFT Add Binding")
plt.plot(x, y1, label="RAFT Query Binding")
plt.plot(x, y2, label="RAFT Sign Binding")
plt.plot(x, y3, label="RAFT Query Sign Binding")

plt.plot(x, y4, label="IBFT Add Binding")
plt.plot(x, y5, label="IBFT Query Binding")
plt.plot(x, y6, label="IBFT Sign Binding")
plt.plot(x, y7, label="IBFT Query Sign Binding")

plt.xlabel('Datum')
plt.xscale('log')

plt.ylabel('Transactions')
plt.ylim([0,260])


plt.title("Average Transaction Per Second")

plt.legend()

plt.show()