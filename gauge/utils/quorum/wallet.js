'use strict';

const path = require('path');
const fs = require('fs');
const process = require('process');
const Web3 = require('web3');
const homedir = require('os').homedir();

let configPath;

if (process.argv.length < 3) {
    configPath = path.join(__dirname, '../quorum.json');
} else {
    configPath = path.join(__dirname, process.argv[2]);
}

let quorumConfig = require(configPath);

const nodeToConnect = process.env.CURRENT_NODE ? process.env.CURRENT_NODE : 'development';
const nodeUrl = quorumConfig.quorum.network[nodeToConnect].httpProvider.url;
const web3 = new Web3(new Web3.providers.HttpProvider(nodeUrl));

fs.readFile(homedir + "/quorum-examples/examples/7nodes/keys/key1", (err, data) => {
    if (err) {
        console.log("No key found", err)
    } else {
        let en_key = data.toString();
        let dc_key = web3.eth.accounts.decrypt(en_key, '')
        console.log('Private Key', dc_key.privateKey);
        console.log('Public Key', dc_key.address);
    }
});
