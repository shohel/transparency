/**
* Original work Copyright 2017 HUAWEI. All Rights Reserved.
*
* Modified work Copyright Persistent Systems 2018. All Rights Reserved.
*
* SPDX-License-Identifier: Apache-2.0
*/

'use strict';

module.exports.info = 'opening accounts';

const accounts = [];
let msg;
let bc;
let contx;
module.exports.init = (blockchain, context, args) => {
    if (!args.hasOwnProperty('message')) {
        return Promise.reject(new Error('simple.open - message is missed in the arguments'));
    }
    msg = args['message'].toString();
    bc = blockchain;
    contx = context;
    return Promise.resolve();
};

module.exports.run = () => {
    let newAcc = 'accounts_' + accounts.length;
    accounts.push(newAcc);
    return bc.invokeSmartContract(contx, 'greeter', 'v0', [{verb: 'setMessage'}, {message: msg}], 120);
};

module.exports.end = (results) => Promise.resolve(accounts);
