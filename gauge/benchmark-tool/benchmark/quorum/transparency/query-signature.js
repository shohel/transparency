/**
* Original work Copyright 2017 HUAWEI. All Rights Reserved.
*
* Modified work Copyright Persistent Systems 2018. All Rights Reserved.
*
* SPDX-License-Identifier: Apache-2.0
*/

'use strict';

const logger = require('../../../src/comm/util');
const cmn = require('./common.js')
module.exports.info = 'Query Signature';

let bc;
let contx;

module.exports.init = (blockchain, context, args) => {
    bc = blockchain;
    contx = context;
    return Promise.resolve();
};

module.exports.run = (iteration) => {
    let imsi = iteration.toString();
    let privateIndexInHex = cmn.privateIndex(imsi);
    let imsiSigner = cmn.signerIMSIAccount(imsi);
    return bc.queryState(contx, 'BindingRegistry', 'v0', [privateIndexInHex, imsiSigner], 'lookupBindingSignature');
};

module.exports.end = function(results) {
    return Promise.resolve();
}
