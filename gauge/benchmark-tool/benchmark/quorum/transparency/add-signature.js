/**
* Original work Copyright 2017 HUAWEI. All Rights Reserved.
*
* Modified work Copyright Persistent Systems 2018. All Rights Reserved.
*
* SPDX-License-Identifier: Apache-2.0
*/

'use strict';

const logger = require('../../../src/comm/util');
const cmn = require('./common')
module.exports.info = 'Sign Binding';

let bc;
let contx;
module.exports.init = (blockchain, context, args) => {
    bc = blockchain;
    contx = context;
    return Promise.resolve();
};

module.exports.run = async (iteration) => {
    let imsi = iteration.toString();
    let privateIndexInHex = cmn.privateIndex(imsi);
    let imsiSigner = cmn.signerIMSIAccount(imsi);
    
    let currentCommitment, earlierCommitment, nonce;
    let signBinding = bc.queryState(contx, 'BindingRegistry', 'v0', [privateIndexInHex], 'lookupBindingCommitment').then((data, err)=> {
        if (err) {
            console.log('Sorry failed to get binding commitment', err)
        } else {
            currentCommitment = data.result[0];
            earlierCommitment = data.result[1];
            nonce = data.result[2].toString();
            return cmn.signatureRPC(privateIndexInHex, nonce, currentCommitment, earlierCommitment);
        }
    }).then((result) => {
        logger.info('signature', result);
        return result;
    });
    
    let signature = await signBinding;
    return bc.invokeSmartContract(contx, 'BindingRegistry', 'v0', [{verb: 'addSign'}, {imsiIndex: privateIndexInHex, imsiAddress: imsiSigner, sign: signature}], 120);
};

module.exports.end = (results) => {
    logger.info('signature binding result', results);
    return Promise.resolve()
};
