'use strict'

const EthUtil = require('ethereumjs-util');
const signerPrivateKey = 'e6181caaffff94a09d7e332fc8da9884d99902c7874eb74354bdcadf411929f1';
const signerAccount = '0xed9d02e382b34818e88B88a309c7fe71E65f419d';

const privateIndex =  (imsi) => {
    return EthUtil.bufferToHex(EthUtil.keccak(imsi));
};

const calculateCommitment = (imsi, eid, profile) => {
    return EthUtil.bufferToHex(EthUtil.hashPersonalMessage(new Buffer(imsi), new Buffer(eid), new Buffer(profile)));
};

const signatureRPC = (index, nonce, curCmt, erCmt) => {
    let hashToSign = EthUtil.bufferToHex(EthUtil.hashPersonalMessage(new Buffer(index), new Buffer(nonce), new Buffer(curCmt), new Buffer(erCmt)));

    let signature = EthUtil.ecsign(EthUtil.hashPersonalMessage(new Buffer(hashToSign)), new Buffer(signerPrivateKey, 'hex'));

    let signatureRPC =EthUtil.toRpcSig(signature.v, signature.r, signature.s);

    return signatureRPC.substring(0,64);
};

const signerIMSIAccount = (imsiIndex) => {
    return signerAccount.concat(imsiIndex);
};

module.exports = {
    privateIndex,
    calculateCommitment,
    signatureRPC,
    signerIMSIAccount
};
