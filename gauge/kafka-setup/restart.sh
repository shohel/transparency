#!/bin/bash
docker-compose -f docker-compose-kafka.yaml  down
docker volume prune -f
docker-compose -f docker-compose-kafka.yaml up -d
